using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BarraEscudo : MonoBehaviour
{

    [SerializeField]
    private ScriptableInteger escudoCharacter;

    [SerializeField]
    private Image barra;

    void Start()
    {
        ModificarVida();
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void ModificarVida()
    {
        barra.fillAmount = escudoCharacter.valorActual / escudoCharacter.valorTotal;
    }
}
