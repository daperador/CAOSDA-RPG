using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Rendering.Universal;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(Pathfinding))]
public class GameManager : MonoBehaviour
{
    [SerializeField]
    private GameEvent m_OnMenuOpened;
    [SerializeField]
    private GameEvent m_OnMenuClosed;
    [SerializeField]
    private GameEvent m_OnHouseOpened;
    [SerializeField]
    private GameEvent m_OnHouseClosed;
    [SerializeField]
    public Casa casa;
    private bool m_ActiveMenu = false;
    private bool isNight = false;
    public static GameManager Instance { get; private set; }

    public GameObject m_ElementASpawnejar;
    [SerializeField]
    private float m_SpawnWait = 30f;
    public CharacterController cc;

    [SerializeField]
    private Transform [] Spawners;
    [SerializeField] Light2D luzGlobal;
    [SerializeField] private GameEvent m_escudoCharacter;

    private Coroutine corutinaDay;
    private Coroutine corutinaNight;
    [SerializeField] private Color colorDay;
    [SerializeField] private Color colorNight;
    [SerializeField] private GameObject[] lucesMapa;



    [Header ("Eventos Seguimiento Zombies")]
    [SerializeField] private GameEvent noChaseZombie;
    [SerializeField] private GameEvent chaseZombie;



    [SerializeField] private AudioSource audioSource;


    [SerializeField] private ScriptableInteger m_characterMoney;
    int earn = 10;

    /*public int Money
    {
        get { return m_dinero; }
        set { m_dinero = value; }

    }*/
    private int m_dineroCasa = 0;
    int earnCasa = 1;
    public int MoneyCasa
    {
        get { return m_dineroCasa; }
        set { m_dineroCasa = value; }

    }


    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(this);
        }
        else
        {
            Instance = this;
        }

    }
    private void Start()
    {
        ApagarLuces();
        noChaseZombie.Raise();

        //luzGlobal.intensity = 1;

        corutinaNight = StartCoroutine(Noche());

        m_OnMenuClosed.Raise();
        m_OnHouseClosed.Raise();
        cc.OnPlayerDeath += PlayerDead;

        for (int i = 0; i < Spawners.Length; i++) { 
           StartCoroutine(SpawnCoroutine(Spawners[i]));
        }
        StartCoroutine(IncreaseDifficulty());
        StartCoroutine(GanarDineroCasa());
    }


     public void Load(CharacterData characterData)
    {
        cc.m_healthScriptable.valorActual = characterData.health;
        cc.m_escudoScriptable.valorActual = characterData.shield;
        cc.transform.position = characterData.position;
        m_escudoCharacter.Raise();
        Debug.Log("Load Data");
    }

    private void PlayerDead()
    {
        StopAllCoroutines();
        SceneManager.LoadScene("GameOver");
    }
    void Update()
    {

        /*if (luzGlobal.intensity >= 1f)
        {
            luzGlobal.color = colorDay;
            for (int i = 0; i < lucesMapa.Length; i++)
            {
                lucesMapa[i].SetActive(false);
            }
        }*/



        if (Input.GetKeyDown(KeyCode.Escape))
        {
            m_ActiveMenu = !m_ActiveMenu;
            if (m_ActiveMenu)
                m_OnMenuOpened.Raise();
            else
                m_OnMenuClosed.Raise();
        }



        if (casa.CasaOpened)
        {
            m_OnHouseOpened.Raise();

            if (Input.GetKeyDown(KeyCode.Space))
            {
                m_OnHouseClosed.Raise();
                casa.CasaOpened = false;
            }
        }

	if (Input.GetKeyDown(KeyCode.G)) {

            SaveManager.SavePlayerData(cc);
            Debug.Log("Save Data");
            
        }

        if (Input.GetKeyDown(KeyCode.E))
        {
            SaveManager.LoadPlayerData();
        }
	

    }

    public void FindPath(Vector3 origen, Vector3 destino, out List<Vector3> camino)
    {
        GetComponent<Pathfinding>().FindPathWorldSpace(origen, destino, out camino);    
    }

    IEnumerator SpawnCoroutine(Transform PositionSpawn)
    {

        
        while (true)
        {

            GameObject spawned = Instantiate(m_ElementASpawnejar);
            spawned.GetComponent<EnemigoControlador>().cc = cc;
            spawned.transform.position = PositionSpawn.position;
            yield return new WaitForSeconds(m_SpawnWait);
        }
    }
    IEnumerator IncreaseDifficulty()
    {
        while (true)
        {
            yield return new WaitForSeconds(60);
            m_SpawnWait -= 0.5f;

            if (m_SpawnWait <= 3f)
            {
                m_SpawnWait = 3f;
            }
        }

    }

    IEnumerator Noche() {
        yield return new WaitForSeconds(120);
        while (true) {
            if (luzGlobal.intensity > 0.50)
            {
                luzGlobal.intensity -= 0.05f;
            }
            else {
                break;
            }
            yield return new WaitForSeconds(1);
        }
        audioSource.Play();
        luzGlobal.color = colorNight;
        EncenderLuces();
        chaseZombie.Raise();

        StopCoroutine(corutinaNight);
        corutinaDay = StartCoroutine(Dia());
        
    
    
    }


    IEnumerator Dia()
    {
        yield return new WaitForSeconds(240);
        ApagarLuces();
        audioSource.Stop();
        while (true)
        {
            if (luzGlobal.intensity < 1)
            {
                luzGlobal.intensity += 0.05f;
            }
            else
            {
                break;
            }
            yield return new WaitForSeconds(1);
        }
        luzGlobal.color = colorDay;
        noChaseZombie.Raise();
        StopCoroutine(corutinaDay);
        corutinaNight = StartCoroutine(Noche());

    }


    private void ApagarLuces() {

            for (int i = 0; i < lucesMapa.Length; i++)
            {
                lucesMapa[i].SetActive(false);
            }
       
    }

    private void EncenderLuces()
    {

        for (int i = 0; i < lucesMapa.Length; i++)
        {
            lucesMapa[i].SetActive(true);
        }

    }

    public void GanarDinero()
    {
        m_characterMoney.valorActual += earn;
    }

    public void RecolectarDineroCasa()
    {
        m_characterMoney.valorActual += m_dineroCasa;
        m_dineroCasa = 0;
    }

    IEnumerator GanarDineroCasa()
    {
        while (true)
        {
            yield return new WaitForSeconds(3);
            m_dineroCasa += earnCasa;
        }
    }
}
