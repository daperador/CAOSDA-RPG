using UnityEngine;

[CreateAssetMenu(fileName = "GameEventItem", menuName = "GameEvent/GameEvent - Item")]
public class GameEventItem : GameEvent<Item> { }

