using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dinero : MonoBehaviour
{
    private TMPro.TextMeshProUGUI m_Text;
    [SerializeField] private ScriptableInteger m_characterMoney;
    // Start is called before the first frame update
    void Start()
    {
        m_Text = GetComponent<TMPro.TextMeshProUGUI>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        m_Text.text = "Gold: " + m_characterMoney.valorActual.ToString();
    }
}
