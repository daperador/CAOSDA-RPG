using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Nivel : MonoBehaviour
{
    [SerializeField]
    private TMPro.TextMeshProUGUI m_Text;
    [SerializeField] private ScriptableInteger m_levelCharacter;
    // Start is called before the first frame update
    void Start()
    {
        m_Text = GetComponent<TMPro.TextMeshProUGUI>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        m_Text.text = m_levelCharacter.valorActual.ToString();
    }
}
