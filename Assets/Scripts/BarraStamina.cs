using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BarraStamina : MonoBehaviour
{

    [SerializeField]
    private ScriptableInteger stamina;

    [SerializeField]
    private Image barra;


    // Start is called before the first frame update
    void Start()
    {
        stamina.valorActual = stamina.valorTotal;
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    public void ModificarStamina()
    {
        barra.fillAmount = stamina.valorActual / stamina.valorTotal;
    }

}
