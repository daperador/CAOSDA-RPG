using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RecolectarDinero : MonoBehaviour
{
    [SerializeField]
    private GameEvent OnRecolectMoney;

    public void raiseRecolectar()
    {
        OnRecolectMoney.Raise();
    }
}
