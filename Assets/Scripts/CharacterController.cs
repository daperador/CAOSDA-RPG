using System.Collections;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CharacterController : MonoBehaviour
{
    Rigidbody2D rb;
    [SerializeField] private float m_velocity = 1;

    private bool activeInv = false;
    [SerializeField] private Inventory m_inventory;

    private Camera cam;
    [SerializeField, Range(1f, 20f)] private float rotationSpeed;

    private Animator m_animator;

    private float m_nextFire;
    public float FireRate = 2.0f;
    public GameObject Bullet;
    public float BulletSpeed;
    public string estatAnim;
    public bool death;



    public delegate void PlayerDeath();
    public event PlayerDeath OnPlayerDeath;
    private Vector2 movimiento;

    public Vector2 movimientoGet
    {
        get { return movimiento; }
        set { movimiento = value; }
    }

    [SerializeField] private GameObject WaypointMina1;
    [SerializeField] private GameObject WaypointMina2;
    [SerializeField] private SpriteRenderer sr;
    [SerializeField] public ScriptableInteger m_healthScriptable;
    [SerializeField] public ScriptableInteger m_staminaScriptable;
    [SerializeField] public ScriptableInteger m_escudoScriptable;
    [SerializeField] public ScriptableInteger m_levelCharacter;

    [Header("Eventos Barra")]
    [SerializeField] private GameEvent m_damageCharacter;
    [SerializeField] private GameEvent m_staminaCharacter;
    [SerializeField] private GameEvent m_escudoCharacter;
    //[SerializeField] private GameObject lightCharacter;


    private CircleCollider2D boxCharacter;



    private float GranadaRate = 5.0f;
    private int numgranadas;
    [SerializeField] public GameObject Granada;
    private bool activeHouse;



    private bool canMove = true;
    private bool moveStamina = true;
    private int staminaCharacter = 10;
    private int vidaCharacter = 10;
    private bool stateCura = false;
    private bool canAnimation = true;
    private bool tengoEscudo = false;
    private int tiempoCuracion = 3;


    private Coroutine corutinaSangrado;


    [SerializeField] private GameEvent LevelUp;
    [SerializeField] private ScriptableInteger exp;

    [SerializeField] private GameEvent ModifyExp;

    private int m_nivel = 0;

    public int Nivel
    {
        get { return m_nivel; }
        set { m_nivel = value; }

    }

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        m_animator = GetComponent<Animator>();
        cam = Camera.main;
        death = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (activeInv || death || activeHouse || !canMove)
            return;
        m_nextFire = m_nextFire + Time.fixedDeltaTime;

        if (Input.GetMouseButton(1) && m_nextFire > FireRate)
        {
            Vector3 m_mousePosition = Input.mousePosition;
            m_mousePosition = Camera.main.ScreenToWorldPoint(m_mousePosition);
            m_mousePosition.z = 0;

            float m_fireAngle = Vector2.Angle(m_mousePosition - this.transform.position, Vector2.up);

            if (m_mousePosition.x > this.transform.position.x)
            {
                m_fireAngle = -m_fireAngle;
            }

            m_nextFire = 0;

            GameObject m_bullet = Instantiate(Bullet, transform.position, Quaternion.identity) as GameObject;

            m_bullet.GetComponent<Rigidbody2D>().velocity = ((m_mousePosition - transform.position).normalized * BulletSpeed);

            m_bullet.transform.eulerAngles = new Vector3(0, 0, m_fireAngle);

        }

        if (numgranadas < 3)
        {
            if (Input.GetKeyDown(KeyCode.Q) && m_nextFire > GranadaRate && numgranadas < 3)
            {
                m_nextFire = 0;
                GameObject m_granada = Instantiate(Granada, transform.position, Quaternion.identity) as GameObject;
                numgranadas++;
            }

        }

        ControlarStamina();

        float moveX = Input.GetAxisRaw("Horizontal");
        float moveY = Input.GetAxisRaw("Vertical");
        movimiento = new Vector2(moveX, moveY).normalized;

        /*if (moveStamina && canAnimation == true)
        {
            m_animator.SetFloat("Horizontal", moveX);
            m_animator.SetFloat("Vertical", moveY);
            m_animator.SetFloat("Speed", movimiento.sqrMagnitude);
        }*/

        m_animator.SetFloat("Horizontal", moveX);
        m_animator.SetFloat("Vertical", moveY);
        m_animator.SetFloat("Speed", movimiento.sqrMagnitude);


    }
    private void FixedUpdate()
    {
        rb.MovePosition(rb.position + movimiento.normalized * m_velocity * Time.fixedDeltaTime);
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Enemigo")
        {
            if (m_escudoScriptable.valorActual <= 0)
            {
                tengoEscudo = false;
            }
            if (tengoEscudo == false)
            {
                vidaCharacter--;
                m_healthScriptable.valorActual--;
                m_damageCharacter.Raise();
                if (corutinaSangrado.IsUnityNull())
                    corutinaSangrado = StartCoroutine(SangradoCharacter());

            }
            else
            {
                m_inventory.TakeRandomEquipment();
                m_escudoScriptable.valorActual--;
                m_escudoCharacter.Raise();
            }

        }

        if (collision.gameObject.tag == "escudo")
        {
            tengoEscudo = true;
            if (m_escudoScriptable.valorActual < m_escudoScriptable.valorTotal)
            {
                m_escudoScriptable.valorActual += 10;
                m_escudoCharacter.Raise();
            }
        }


        if (collision.gameObject.tag == "botiquinItem")
        {

            tiempoCuracion = 3;

            if (m_healthScriptable.valorActual < m_healthScriptable.valorTotal)
            {
                StopCoroutine(corutinaSangrado);
                StartCoroutine(CuracionCharacter());
            }
            else
            {
                print("tienes la vida al completo");
            }
        }
        if (collision.gameObject.tag == "Mine")
        {
            SceneManager.LoadScene("MineScene");
        }
        if (collision.gameObject.tag == "BuildBaixar")
        {

            transform.position = WaypointMina2.transform.position;
            sr.sortingOrder = 3;
        }

    }

    public void OnMenuOpened()
    {
        activeInv = true;
    }

    public void OnMenuClosed()
    {
        activeInv = false;
    }
    public void OnHouseOpened()
    {
        activeHouse = true;
    }

    public void OnHouseClosed()
    {
        activeHouse = false;
    }

    /*public void NoChase()
    {
        boxCharacter.enabled = false;
    }*/

    public void OnUseMedkit()
    {
        if (m_healthScriptable.valorActual < m_healthScriptable.valorTotal)
        {
            tiempoCuracion = 3;
            if (!corutinaSangrado.IsUnityNull())
                StopCoroutine(corutinaSangrado);
            StartCoroutine(CuracionCharacter());
        }
    }

    public void OnEquipEquipment()
    {
        print("OnEquipEquipment");
        tengoEscudo = true;
        if (m_escudoScriptable.valorActual < m_escudoScriptable.valorTotal)
        {
            m_escudoScriptable.valorActual += 10;
            m_escudoCharacter.Raise();
        }
    }

    void Death()
    {
        OnPlayerDeath.Invoke();
        death = true;

    }


    IEnumerator SangradoCharacter()
    {

        while (true)
        {
            if (m_healthScriptable.valorActual >= 0)
            {
                m_healthScriptable.valorActual -= 0.5f;
                m_damageCharacter.Raise();
            }
            else
            {
                break;
            }
            yield return new WaitForSeconds(1);
        }
    }

    IEnumerator CuracionCharacter()
    {

        while (true)
        {
            if (tiempoCuracion > 0)
            {
                m_healthScriptable.valorActual += 1f;
                m_damageCharacter.Raise();
                tiempoCuracion = tiempoCuracion - 1;
            }
            else
            {
                break;
            }
            yield return new WaitForSeconds(1);
        }
        Debug.Log("sale");

    }




    public void ControlarStamina()
    {
        Vector2 v = new Vector2(0, 0);

        if (movimiento != v)
        {
            if (m_staminaScriptable.valorActual > 0 && moveStamina != false)
            {
                m_staminaScriptable.valorActual -= 1 * Time.deltaTime * 0.5f;
                m_staminaCharacter.Raise();
            }
        }
        else if (movimiento == v)
        {
            if (m_staminaScriptable.valorActual < m_staminaScriptable.valorTotal)
            {
                m_staminaScriptable.valorActual += 1 * Time.deltaTime * 1;
                m_staminaCharacter.Raise();
            }

        }
        if (m_staminaScriptable.valorActual <= 0)
        {
            //canAnimation = false;
            //m_animator.gameObject.GetComponent<Animator>().enabled = false;
            m_velocity = 1;
        }

        else
        {
            //m_animator.gameObject.GetComponent<Animator>().enabled = true;
            m_velocity = 2;
            /*moveStamina = true;
            canAnimation = true;*/

        }

    }



    /*IEnumerator DelayStamina() {
        yield return new WaitForSeconds(10);
        moveStamina = true;
    }*/

    /*IEnumerator Espera()
    {
        yield return new WaitForSeconds(5);
        m_velocity = 2;
    }*/

    public void GanarExp()
    {
        exp.valorActual += 20;

        if (exp.valorActual >= exp.valorTotal)
        {
            exp.valorActual -= exp.valorTotal;
            exp.valorTotal += 20;

            LevelUp.Raise();
        }
        ModifyExp.Raise();
    }

    public void SubirNivel()
    {
        m_levelCharacter.valorActual += 1;
        m_healthScriptable.valorTotal += 5;
    }

}
