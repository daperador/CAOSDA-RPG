using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class SaveData
{
    public CharacterData character;
    public List<SoldierData> soldiers;

    public SaveData(CharacterController charController, SoldadosController[] soldiersControllers)
    {
        character = new CharacterData(charController);
        soldiers = new List<SoldierData>();
        foreach(SoldadosController soldado in soldiersControllers)
        {
            soldiers.Add(new SoldierData(soldado));
        }
    }
}

[Serializable]
public class CharacterData
{
    public float health;
    public float shield;
    public Vector3 position;

    public CharacterData(CharacterController characterController)
    {

        health = characterController.m_healthScriptable.valorActual;
        shield = characterController.m_escudoScriptable.valorActual;
        position = characterController.transform.position;

    }
}

[Serializable]
public class SoldierData
{
    public Vector3 position;

    public SoldierData(SoldadosController soldadoController)
    {
        position = soldadoController.transform.position;
    }
}