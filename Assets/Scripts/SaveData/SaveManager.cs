using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using OdinSerializer;

public static class SaveManager
{
    public static void SavePlayerData(CharacterController characterController) {

        SoldadosController[] soldados = GameObject.FindObjectsOfType<SoldadosController>();

        SaveData savedata = new SaveData(characterController, soldados);


        string charactherPath = Path.Combine(Application.persistentDataPath, "characterData.dat");

        byte[] serializedData = SerializationUtility.SerializeValue<SaveData>(savedata, DataFormat.JSON);
        string base64 = System.Convert.ToBase64String(serializedData);

        File.WriteAllText(charactherPath, base64);
    }
    public static void LoadPlayerData()
    {
        string dataPath = Path.Combine(Application.persistentDataPath, "characterData.dat");
        if (File.Exists(dataPath)) {

            string newBase64 = File.ReadAllText(dataPath);
            byte[] serializedData = System.Convert.FromBase64String(newBase64);

            SaveData savedata = SerializationUtility.DeserializeValue<SaveData>(serializedData, DataFormat.JSON);

            GameObject.FindObjectOfType<GameManager>().Load(savedata.character);
            GameObject.FindObjectOfType<CrearSoldado>().Load(savedata.soldiers);
        }
        else
        {
            Debug.Log("ARCHIVO NO ENCONTRADO");
        }

    }

}
