using UnityEngine;

public class BulletController : MonoBehaviour
{
    private void OnBecameInvisible()
    {
        Destroy(this.gameObject);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag != "Character")
            Destroy(this.gameObject);

    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        /*if (collision.gameObject.tag == "House")
            Destroy(this.gameObject);
        if (collision.gameObject.tag == "Mountain-tree")
            Destroy(this.gameObject);*/
        if (collision.gameObject.tag != "Character" && collision.gameObject.tag != "Soldado")
            Destroy(this.gameObject);
    }
}
