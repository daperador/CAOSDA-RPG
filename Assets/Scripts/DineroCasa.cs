using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DineroCasa : MonoBehaviour
{
    [SerializeField]
    private GameManager m_GameManager;
    private TMPro.TextMeshProUGUI m_Text;

    void Start()
    {
        m_Text = GetComponent<TMPro.TextMeshProUGUI>();
    }

    // Update is called once per frame
    void Update()
    {
        m_Text.text = m_GameManager.MoneyCasa.ToString();
    }
}
