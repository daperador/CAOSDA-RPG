using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BarraExp : MonoBehaviour
{
    [SerializeField]
    private ScriptableInteger exp;

    [SerializeField]
    private Image barra;

    // Start is called before the first frame update
    void Start()
    {
        //exp.valorActual = 0;
        //exp.valorTotal = 100;
        ModificarExp();
    }
    public void ModificarExp()
    {
        if((exp.valorActual / exp.valorTotal)<1)
        barra.fillAmount = exp.valorActual / exp.valorTotal;
    }
}
