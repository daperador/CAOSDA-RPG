using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ShopController : MonoBehaviour
{
    [SerializeField] private ShopItems m_shop;
    [SerializeField] private TextMeshProUGUI m_textItem;
    [SerializeField] private TextMeshProUGUI m_priceItem;
    [SerializeField] private GameObject m_imageItem;
    [SerializeField] private GameObject m_vender;
    [SerializeField] private GameEvent m_OnMenuOpen;
    [SerializeField] private GameEvent m_OnMenuClose;
    [SerializeField] private Inventory inventory;
    [SerializeField] private TextMeshProUGUI m_textState;
    [SerializeField] private GameObject m_ItemShop;
    [SerializeField] private GameObject m_ExitMenu;
    private ItemsShop itemshop;
    private Item m_itemToBuy;
    private bool activeMenu;
    private bool activeExitMenu;
    private void Awake()
    {
        SetMenuElementsActive(false);
        SetMenuElementsExitActive(false);
        activeExitMenu = false;
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape)) { 
            activeExitMenu = !activeExitMenu;
            if (activeExitMenu) { 
                SetMenuElementsActive(false);
                SetMenuElementsExitActive(true);
            }
            else { 
                SetMenuElementsExitActive(false);
            }

        }

    }
    public void OpenShopMenu(Item item)
    {
        m_itemToBuy = item;
        m_OnMenuOpen.Raise();
        SetMenuElementsActive(true);
        itemshop = m_shop.GetItem(item);
        m_textItem.text = item.name;
        m_priceItem.text = itemshop.price.ToString()+"c.";
        m_imageItem.GetComponent<Image>().sprite = item.sprite;
    }

    private void SetMenuElementsActive(bool state)
    {
        activeMenu = state;
        for (int i = 0; i < m_ItemShop.transform.childCount; ++i)
            m_ItemShop.transform.GetChild(i).gameObject.SetActive(state);
        m_textState.text = "";
    }
    private void SetMenuElementsExitActive(bool state)
    {
        activeMenu = state;
        for (int i = 0; i < m_ExitMenu.transform.childCount; ++i)
            m_ExitMenu.transform.GetChild(i).gameObject.SetActive(state);
        m_textState.text = "";
    }

    public void OnMenuClose()
    {
        m_OnMenuClose.Raise();
        SetMenuElementsActive(false);
    }

    public void BuyItem()
    {
        print(m_itemToBuy.ToString());
        m_textState.text = inventory.BuyItem(m_itemToBuy, itemshop.price);
    }
    public void ExitShop()
    {
        SetMenuElementsActive(false);
        SceneManager.LoadScene("GameCarlos");
    }

    public void NoExitShop()
    {
        SetMenuElementsExitActive(false);
    }
}
