using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    [SerializeField] private Transform m_Character;
    public Transform Character { get { return m_Character; } set { m_Character = value; } }

    [Tooltip("Time it takes to reach the target object")]
    [SerializeField]
    private float m_FollowDelay = 1f;
    private Vector3 m_VectorSpeed = Vector3.zero;

    [SerializeField]
    private Vector3 m_Offset;

    private float zoom = 5f;
    void Update()
    {
        if (Character != null)
        {
            float transformX = Character.transform.position.x;
            float transformY = Character.transform.position.y;

            transform.position = Vector3.SmoothDamp(transform.position, new Vector3(transformX, transformY + 2, transform.position.z) + m_Offset, ref m_VectorSpeed, m_FollowDelay);
        }

        if (GetComponent<Camera>().orthographic)
        {
            if (GetComponent<Camera>().orthographicSize > 2f && GetComponent<Camera>().orthographicSize < 8f)
                GetComponent<Camera>().orthographicSize -= Input.GetAxis("Mouse ScrollWheel") * zoom;
            if (GetComponent<Camera>().orthographicSize <= 2)
                GetComponent<Camera>().orthographicSize = 2.5f;
            if (GetComponent<Camera>().orthographicSize >= 8)
                GetComponent<Camera>().orthographicSize = 7.5f;
        }
        else
        {
            if (GetComponent<Camera>().fieldOfView > 2f && GetComponent<Camera>().fieldOfView < 8f)
                GetComponent<Camera>().fieldOfView -= Input.GetAxis("Mouse ScrollWheel") * zoom;
            if (GetComponent<Camera>().fieldOfView <= 2)
                GetComponent<Camera>().fieldOfView = 2.5f;
            if (GetComponent<Camera>().fieldOfView >= 8)
                GetComponent<Camera>().fieldOfView = 7.5f;
        }
    }
}
