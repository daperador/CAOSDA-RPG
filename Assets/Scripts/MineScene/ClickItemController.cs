using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ClickItemController : MonoBehaviour
{
    [SerializeField] private Item m_item;
    [SerializeField] private GameEventItem m_onClickItem;
    private void Awake()
    {
        GetComponent<SpriteRenderer>().sprite = m_item.sprite;
    }
    private void OnMouseDown()
    {
        m_onClickItem.Raise(m_item);
    }
}
