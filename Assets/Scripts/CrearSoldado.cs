using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrearSoldado : MonoBehaviour
{
    [SerializeField]
    private GameObject m_ElementASpawnejar;

    public void CreateSoldier()
    {
        GameObject spawned = Instantiate(m_ElementASpawnejar);
        spawned.transform.position = this.transform.position;
        print("spawned");
    }

    public void Load(List<SoldierData> soldados)
    {
        BorrarSoldados();
        foreach (SoldierData s in soldados)
        {
            GameObject spawned = Instantiate(m_ElementASpawnejar);
            spawned.transform.position = s.position;
            print("spawned");
        }   
    }

    private void BorrarSoldados()
    {
        SoldadosController[] soldados = GameObject.FindObjectsOfType<SoldadosController>();
        foreach(SoldadosController s in soldados)
            Destroy(s.gameObject);
    }
}
