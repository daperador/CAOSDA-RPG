using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryController : MonoBehaviour
{
    [SerializeField] public List<GameObject> InventoryGUI = new List<GameObject>();
    [SerializeField] private List<GameObject> InventoryEquipmentGUI = new List<GameObject>();
    [SerializeField] Inventory InventoryScriptable;
    [SerializeField] GameObject equipmentInventory;
    /*[SerializeField] private GameObject m_level;
    [SerializeField] private GameObject m_money;
    [SerializeField] private GameObject m_background;*/
    [SerializeField] private List<Sprite> sprites = new List<Sprite>();
    [SerializeField] private GameObject selector;
    [SerializeField] private TMPro.TextMeshProUGUI quantityItem;
    [SerializeField] private GameEvent m_medkitHealth;
    [SerializeField] private GameEvent m_equipEvent;
    [SerializeField] private GameObject m_background;
    private int id;
    private bool activeMenu;

    private void Awake()
    {
        InventoryScriptable.OnStartGame();
        RenewInventoryEquipment();
        SetMenuElementsActive(false);
    }

    public void Update()
    {
        if (!activeMenu)
            return;

        if (Input.GetKeyDown(KeyCode.RightArrow) && id < InventoryGUI.Count - 1)
        {
            id++;
        }
        if (Input.GetKeyDown(KeyCode.LeftArrow) && id > 0)
        {
            id--;
        }
        if (Input.GetKeyDown(KeyCode.UpArrow) && id > 3)
        {
            id -= 6;
        }
        if (Input.GetKeyDown(KeyCode.DownArrow) && id < 18)
        {
            id += 6;
        }
        if (id >= 0 && id < 18)
        {
            selector.transform.position = InventoryGUI[id].transform.position;
            if (id >= 12 && id < 18)
            {
                selector.GetComponent<Image>().sprite = sprites[0];
            }
            else
            {
                selector.GetComponent<Image>().sprite = sprites[1];
            }
            if (InventoryScriptable.InventoryItems.Count > id)
            {
                quantityItem.text = InventoryScriptable.InventoryItems[id].quantity.ToString();
            }
            else
            {
                quantityItem.text = "";
            }

        }
        if (Input.GetKeyDown(KeyCode.Return))
        {
            for (int i = 0; i < transform.childCount; ++i)
            {
                if (Mathf.Round(InventoryGUI[i].transform.position.x) == Mathf.Round(selector.transform.position.x))
                {
                    if (Mathf.Round(InventoryGUI[i].transform.position.y) == Mathf.Round(selector.transform.position.y))
                    {
                        Debug.Log("Element -> " + InventoryGUI[i].name);
                        if (InventoryGUI[i].tag.Equals("Equipment"))
                        {
                            if (InventoryScriptable.EquipItem(InventoryGUI[i].name))
                                m_equipEvent.Raise();
                            else
                                Debug.Log("No pots equiparte una cosa ja equipada");
                        }
                        else if (InventoryGUI[i].tag.Equals("Material"))
                        {
                            if (InventoryScriptable.InventoryItems[i].name == "Botiquin") { 
                                if (InventoryScriptable.EliminateOrSubtractMaterial(InventoryScriptable.InventoryItems[i] as Material, 1))
                                {
                                    m_medkitHealth.Raise();
                                }
                                else
                                {
                                    Debug.Log("Quantitat insuficient");
                                }
                            }
                            else
                            {
                                Debug.Log("No botiquin");
                            }
                        }

                    }
                }
            }

        }
    }

    public void OnMenuOpened()
    {
        SetMenuElementsActive(true);
        SetExtraElementsActive(false);
    }

    public void OnMenuClosed()
    {
        SetMenuElementsActive(false);
        SetExtraElementsActive(true);
    }

    private void SetMenuElementsActive(bool state)
    {
        activeMenu = state;
        for (int i = 0; i < transform.childCount; ++i)
            transform.GetChild(i).gameObject.SetActive(state);

        GetComponent<Image>().enabled = state;
        selector.gameObject.SetActive(state);
        quantityItem.gameObject.SetActive(state);
        equipmentInventory.SetActive(state);
        m_background.SetActive(state);

    }
    private void SetExtraElementsActive(bool state)
    {
    }
    public void addItemToInventoryGuiMaterial(Material newItem)
    {
        for (int i = 0; i < InventoryGUI.Count - 6; i++)
        {

            if (InventoryGUI[i].GetComponent<Image>().enabled == false)
            {
                InventoryGUI[i].GetComponent<Image>().enabled = true;
                InventoryGUI[i].GetComponent<Image>().sprite = newItem.sprite;
                InventoryGUI[i].gameObject.name = newItem.name;
                InventoryGUI[i].tag = "Material";
                break;
            }


        }
    }

    public void addItemToInventoryGuiEquipment(Equipment newEquipment)
    {
        for (int i = InventoryGUI.Count - 6; i < InventoryGUI.Count; i++)
        {
            if (InventoryGUI[i].GetComponent<Image>().enabled == false)
            {
                InventoryGUI[i].GetComponent<Image>().enabled = true;
                InventoryGUI[i].GetComponent<Image>().sprite = newEquipment.sprite;
                InventoryGUI[i].gameObject.name = newEquipment.name;
                InventoryGUI[i].tag = "Equipment";
                break;
            }
        }

    }
    public void addEquipmentToEquipmentInventory(Equipment equipment)
    {
        Debug.Log(equipment.name);
        if (equipment.name.Equals("CascoAntigas"))
        {
            InventoryEquipmentGUI[0].name = equipment.name;
            InventoryEquipmentGUI[0].GetComponent<Image>().enabled = true;
            InventoryEquipmentGUI[0].GetComponent<Image>().sprite = equipment.sprite;
        }
        if (equipment.name.Equals("ChalecoAntibalas"))
        {
            InventoryEquipmentGUI[1].name = equipment.name;
            InventoryEquipmentGUI[1].GetComponent<Image>().enabled = true;
            InventoryEquipmentGUI[1].GetComponent<Image>().sprite = equipment.sprite;
        }
        if (equipment.name.Equals("PantalonMilitar"))
        {
            InventoryEquipmentGUI[2].name = equipment.name;
            InventoryEquipmentGUI[2].GetComponent<Image>().enabled = true;
            InventoryEquipmentGUI[2].GetComponent<Image>().sprite = equipment.sprite;
        }
        if (equipment.name.Equals("BotasMilitares"))
        {
            InventoryEquipmentGUI[3].name = equipment.name;
            InventoryEquipmentGUI[3].GetComponent<Image>().enabled = true;
            InventoryEquipmentGUI[3].GetComponent<Image>().sprite = equipment.sprite;
        }
    }

    private void VaciarInventarioGUI()
    {
        for (int i = 0; i < InventoryGUI.Count; i++)
        {
            InventoryGUI[i].GetComponent<Image>().enabled = false;
            InventoryGUI[i].GetComponent<Image>().sprite = null;
            InventoryGUI[i].name = "slot " + i;
            InventoryGUI[i].tag = "Untagged";
        }
    }
    private void VaciarInventarioEquipamientoGUI()
    {
        for (int i = 0; i < InventoryEquipmentGUI.Count; i++)
        {
            InventoryGUI[i].GetComponent<Image>().enabled = false;
            InventoryGUI[i].GetComponent<Image>().sprite = null;
            InventoryGUI[i].name = "slot ";
            InventoryGUI[i].tag = "Untagged";
        }
    }

    public void RenewInventoryEquipment()
    {
        //vaciar
        VaciarInventarioGUI();
        VaciarInventarioEquipamientoGUI();
        //pintar
        Debug.Log("si");
        foreach (Item item in InventoryScriptable.InventoryItems)
        {
            if (item.GetType() == typeof(Equipment))
            {
                addItemToInventoryGuiEquipment((Equipment)item);
            }
            else if (item.GetType() == typeof(Material))
            {
                //ComproveIfIsRepeat();
                addItemToInventoryGuiMaterial((Material)item);
            }
        }

        foreach (Equipment equiment in InventoryScriptable.InventoryEquipment)
        {
            addEquipmentToEquipmentInventory(equiment);
        }
    }

    public void RenewInventory()
    {
        SetMenuElementsActive(false);
        RenewInventoryEquipment();
    }
}
