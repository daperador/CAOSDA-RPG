using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[CreateAssetMenu(fileName = "New Inventory", menuName = "InventorySO")]

public class Inventory : ScriptableObject
{
    public GameEvent renewInventoryGUI;
    public GameEvent equipmentEvent;
    public ScriptableInteger characterMoney;
    public ScriptableInteger escudoCharacter;
    public int maxCapacityItems;
    public int maxCapacityEquipment;
    public List<Item> InventoryItems;
    public List<Equipment> InventoryEquipment;

    public void OnStartGame()
    {
        Debug.Log("Inicia juego");
        escudoCharacter.valorActual = 0;
        for (int i = 0; i < InventoryEquipment.Count; i++)
        {
            equipmentEvent.Raise();
        }
    }
    public bool addItem(Material newItem)
    {

        if (InventoryItems.Contains(newItem))
        {
            Debug.Log("Ja ho tens, incrmenta la quantitat");
            newItem.quantity++;
            return true;
        }
        else
        {
            if (InventoryItems.Count < maxCapacityItems)
            {
                Debug.Log("S'ha afegit al inventari");
                InventoryItems.Add(newItem);
                renewInventoryGUI.Raise();


                return true;
            }
            else
            {
                return false;
            }

        }
    }

    public bool addItem(Equipment newItem)
    {
        Equipment existingItem = InventoryItems.FirstOrDefault(item => item.name == newItem.name) as Equipment;
        if (existingItem != null)
        {
            Debug.Log("Ja tens aquest equipat aquest " + newItem.name);
            return false;
        }
        else
        {
            if (InventoryItems.Count < maxCapacityItems)
            {
                InventoryItems.Add(newItem);
                return true;

            }
            return false;
        }
    }

    public bool EquipItem(string equipName)
    {
        foreach (Item equip in InventoryItems)
        {
            if (equip.name == equipName)
            {
                return EquipItem(equip as Equipment);
            }
        }

        return false;
    }

    public bool EquipItem(Equipment newItem)
    {
        if (InventoryEquipment.Contains(newItem))
            return false;

        Equipment inventoryItem = InventoryItems.FirstOrDefault(item => item.name == newItem.name) as Equipment;
        if (inventoryItem != null && InventoryEquipment.Count < maxCapacityEquipment)
        {
            InventoryItems.Remove(newItem);
            newItem.equipat = true;
            InventoryEquipment.Add(newItem);
            renewInventoryGUI.Raise();
            return true;
        }
        else
        {
            return false;
        }

    }

    public bool UnequipItem(Equipment unequippedItem)
    {
        if (InventoryEquipment.Contains(unequippedItem))
            return false;

        Equipment inventoryItem = InventoryItems.FirstOrDefault(item => item.name == unequippedItem.name) as Equipment;
        if (inventoryItem != null)
        {
            return false;
        }
        else
        {
            if (InventoryItems.Count < maxCapacityItems)
            {
                unequippedItem.equipat = false;
                InventoryEquipment.Remove(unequippedItem);
                InventoryItems.Add(unequippedItem);
                renewInventoryGUI.Raise();
                return true;
            }
            else
            {
                return false;
            }

        }

    }

    public bool EliminateOrSubtractMaterial(Material material, int quantityToSubtract)
    {
        if (InventoryItems.Contains(material))
        {
            if (material.quantity >= quantityToSubtract)
            {
                material.quantity -= quantityToSubtract;
                if (material.quantity == 0)
                {
                    InventoryItems.Remove(material);
                }
                renewInventoryGUI.Raise();
                return true;
            }
            return false;
        }
        return false;
    }

    public void CheckStateEquipment(Equipment equipment)
    {
        if (equipment.vida <= 0)
        {
            TakeOfEquipment(equipment);
        }
    }

    public void TakeOfEquipment(Equipment equipment)
    {
        InventoryEquipment.Remove(equipment);
        Debug.Log(equipment.name + " eliminado.");
        renewInventoryGUI.Raise();
    }

    public void TakeRandomEquipment()
    {
        Equipment equipment = null;
        int countRandom = Random.Range(0, InventoryEquipment.Count);
        int count = 0;
        foreach (Equipment equipmentItem in InventoryEquipment)
        {
            if (countRandom == count)
            {
                equipment = equipmentItem;
            }
            count++;
        }
        if (InventoryEquipment.Count > 0)
        {
            equipment.vida--;
            Debug.Log(equipment.name + " " + equipment.vida + " vida.");
            CheckStateEquipment(equipment);
        }
    }

    public string BuyItem(Item item, int price)
    {
        if (item is Equipment)
        {
            if (characterMoney.valorActual >= price)
            {
                characterMoney.valorActual -= price;
                if (addItem(item as Equipment))
                    return "Compra de " + item.name + " feta amb �xit.";
                else
                    return "Error en la compra, ja tens " + item.name + ".";
            }
            else
            {
                return "Error en la compra, no tens suficient diners.";
            }
        }
        else if (item is Material)
        {
            if (characterMoney.valorActual >= price)
            {
                characterMoney.valorActual -= price;
                if (addItem(item as Material))
                    return "Compra de " + item.name + " feta amb �xit.";
                else
                    return "Inventari ple.";
            }
            else
            {
                return "Error en la compra, no tens suficient diners.";
            }
        }
        return "Erros desconegut.";
    }
}
