using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "New Item", menuName = "Item")]

public class Item : ScriptableObject
{
    public enum type
    {
        Equipment, Material
    }
    public Sprite sprite;
    public string name;
    public type t;
    public int quantity;
}
