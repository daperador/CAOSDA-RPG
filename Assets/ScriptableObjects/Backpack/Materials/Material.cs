using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Material", menuName = "MaterialSO")]

public class Material : Item
{
    public GameObject gameObject;
}
