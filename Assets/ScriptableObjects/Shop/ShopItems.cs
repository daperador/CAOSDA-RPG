using System.Collections.Generic;
using System.Linq;

using UnityEngine;
using UnityEngine.UIElements;

[System.Serializable]
public class ItemsShop
{
    public Item item;
    public int price;
    public Image image;
}
[CreateAssetMenu(menuName = "Scriptable Objects/Scriptable Shop")]
public class ShopItems : ScriptableObject
{
    public List<ItemsShop> botiga = new List<ItemsShop>();

    public ItemsShop GetItem(Item item)
    {
        return botiga.FirstOrDefault(itemshop => itemshop.item == item);
    }
}
